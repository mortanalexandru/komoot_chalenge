# komoot_chalenge

Handle incoming event stream and optimize the notification sending process.

## Run instructions
### Run with Docker
Requirements
* Docker

Build image
```jshelllanguage
docker build ./path_to_Dockerfile -t optimize_notifications
```

Start container
```jshelllanguage
docker run optimize_notifications:latest https://static-eu-komoot.s3.amazonaws.com/backend/challenge/notifications.csv
```
 
### Run with Python
Requirements:
* Python 3.4+
* Pip

Install dependencies:
```jshelllanguage
pip install -r requirements.txt
```
Run Python script
```jshelllanguage
python notification_scheduler.py https://static-eu-komoot.s3.amazonaws.com/backend/challenge/notifications.csv
```
