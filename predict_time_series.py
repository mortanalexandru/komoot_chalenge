import numpy as np
from pandas import Series
from statsmodels.tsa.ar_model import AR

def difference(dataset):
    diff = list()
    for i in range(1, len(dataset)):
        value = dataset[i] - dataset[i - 1]
        diff.append(value)
    return np.array(diff)

def predict(coef, history):
    yhat = coef[0]
    for i in range(1, len(coef)):
        yhat += coef[i] * history[-i]
    return yhat

def train_model(historical_values, hour_values):
    series = Series([hour_values[hour] for hour in hour_values], index=hour_values.keys())
    X = difference(series.values)
    train = np.concatenate((historical_values, X)) if historical_values.shape[0] > 0 else X
    model = AR(train)
    model_fit = model.fit(disp=False, maxlag=5)
    coef = model_fit.params
    return coef, train
