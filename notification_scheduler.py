import pandas as pd
import datetime
import time
from math import ceil, floor
import numpy as np
import sys

from predict_time_series import train_model, predict

notification_handled_stats = {"delayed_to_end": 0, "max_delay_in_interval": 0.0, "min_delay_in_interval": 999999.9,
                              "delayed_additional_to_end": 0, "sent_notifications": 0,
                              "sent_notifications_notification_hours": 0, "sent_events": 0,
                              "accumulated_max_delay": 0.0, "days": 0, "accumulated_delay_notification_hours": 0.0}

pending_notification_schedule = {}
pending_notification_user = {}

user_day_notifications = {}
user_daily_events = {}
user_latest_events = {}


config = {"wait_time": 60 * 60, "job_timeout": 5,
          "notifications_time_interval": (datetime.time(9, 00, 00), datetime.time(21, 00, 00)), "max_notifications": 4}

day_split = (config["notifications_time_interval"][1].hour - config["notifications_time_interval"][0].hour) / config[
    'max_notifications']

frequency_prediction_models = {}


class PendingNotification(object):
    def __init__(self, scheduled_timestamp, receiver_id, timestamp_first_tour, tours_number, friend_name, fixed):
        self.scheduled_timestamp = scheduled_timestamp
        self.receiver_id = receiver_id
        self.timestamp_first_tour = timestamp_first_tour
        self.tours_number = tours_number
        self.friend_name = friend_name
        self.fixed = fixed

    def __lt__(self, other):
        return self.timestamp < other.timestamp

    def __repr__(self):
        return 'ScheduledNotification({})'.format(self.timestamp, self.receiver)


class ScheduledJobsExecutor(object):

    def __init__(self, current_date, job_timeout, job, daily_job):
        self.prev_job_run = current_date
        self.current_day = current_date.date()
        self.job = job
        self.job_timeout = job_timeout
        self.daily_job = daily_job

    def __lt__(self, other):
        return self.timestamp < other.timestamp

    def __repr__(self):
        return 'ScheduledNotification({})'.format(self.timestamp, self.receiver)

    def mock_time_passing(self, event_date):
        self.run_scheduled_jobs(event_date)

    def run_scheduled_jobs(self, current_date):
        while self.prev_job_run <= current_date:
            if self.current_day < self.prev_job_run.date():
                self.current_day = self.prev_job_run.date()
                self.daily_job(self.prev_job_run)
            self.job(self.prev_job_run)
            self.prev_job_run += datetime.timedelta(minutes=self.job_timeout)


def handle_pending_notifications(timestamp=None):
    if timestamp is not None and len(pending_notification_schedule) > 0:
        send_expired_notifications(timestamp)
    else:
        send_all_pending_notifications()


def send_expired_notifications(timestamp):
    min_expiring_timestamp = min(pending_notification_schedule.keys())
    if min_expiring_timestamp is not None and timestamp >= min_expiring_timestamp[0]:
        for pending_timestamp, user_id in sorted(pending_notification_schedule.keys(), key=lambda key: key[0]):
            if pending_timestamp <= timestamp:
                expired_notification = get_expired_notification(pending_timestamp, user_id)
                send_notification(expired_notification, timestamp)
            else:
                return


def get_expired_notification(pending_timestamp, user_id):
    del pending_notification_user[user_id]
    return pending_notification_schedule.pop((pending_timestamp, user_id))


def send_all_pending_notifications():
    for pending_timestamp, user_id in sorted(pending_notification_schedule.keys(), key=lambda key: key[0]):
        expired_notification = pending_notification_schedule.pop((pending_timestamp, user_id))
        sent_timestamp = expired_notification.scheduled_timestamp if expired_notification.scheduled_timestamp.time().minute % \
                                                                     config[
                                                                         'job_timeout'] == 0 else expired_notification.scheduled_timestamp + datetime.timedelta(
            minutes=(
                    config['job_timeout'] - expired_notification.scheduled_timestamp.time().minute % config[
                'job_timeout']))
        send_notification(expired_notification, sent_timestamp)
        del pending_notification_user[user_id]


def send_notification(notification, timestamp):
    if notification.tours_number == 1:
        message = "{0} went on a tour".format(notification.friend_name)
    else:
        message = "{0} and {1} others went on a tour".format(notification.friend_name, notification.tours_number - 1)

    delay = (timestamp - notification.timestamp_first_tour).total_seconds()
    print("{0},{1},{2},{3},{4}".format(timestamp.strftime("%m-%d-%Y %H:%M:%S"),
                                       notification.timestamp_first_tour.strftime("%m-%d-%Y %H:%M:%S"),
                                       notification.tours_number, notification.receiver_id, message))
    if notification.receiver_id not in user_day_notifications:
        user_day_notifications[notification.receiver_id] = 0
    user_day_notifications[notification.receiver_id] += 1
    return notification, delay


def add_pending_notification(scheduled_timestamp, user_id, event, fixed):
    pending_notification = PendingNotification(scheduled_timestamp, user_id, event['date'], 1,
                                               event['friend_name'], fixed)
    pending_notification_schedule[(scheduled_timestamp, user_id)] = pending_notification
    pending_notification_user[user_id] = pending_notification


def handle_active_hours_events(event, end_period):
    user_id = event['user_id']
    event_date = event['date']
    wait_time = predict_wait_time(user_id, event)
    scheduled_timestamp, fixed_time = get_scheduled_timestamp(event_date, end_period, user_id, wait_time)
    add_pending_notification(scheduled_timestamp, user_id, event, fixed_time)


def predict_wait_time(user_id, event):
    if user_id in frequency_prediction_models:
        prediction = predict(frequency_prediction_models[user_id]['coef'],
                             frequency_prediction_models[user_id]['history'])
        events_prev_hour, events_current_hour = get_latest_events_count(user_id, event['date'])
        if ceil(events_prev_hour + prediction) <= events_current_hour:
            return 0
    else:
        if user_id in user_day_notifications:
            sent_notifications = user_day_notifications[user_id]
            part_of_day = floor((event['date'].hour - config["notifications_time_interval"][0].hour) / day_split)
            if sent_notifications > part_of_day:
                return config['wait_time']
            else:
                return 0

    return config['wait_time']


def get_latest_events_count(user_id, current_date):
    prev_hour_timestamp = current_date.replace(minute=0, second=0) - datetime.timedelta(hours=1)
    if user_id not in user_daily_events:
        return 0, 0
    else:
        return len([prev_event for prev_event in user_daily_events[user_id] if
                    prev_hour_timestamp == prev_event.replace(minute=0, second=0)]), len(
            [prev_event for prev_event in user_daily_events[user_id] if
             current_date.replace(minute=0, second=0) == prev_event.replace(minute=0, second=0)])


def get_scheduled_timestamp(event_date, end_period, user_id, wait_time):
    scheduled_timestamp = event_date + datetime.timedelta(
        seconds=wait_time)
    if scheduled_timestamp >= end_period:
        return end_period, True
    if user_id in user_day_notifications and user_day_notifications[user_id] > 2:
        notification_handled_stats['delayed_to_end'] += 1
        return end_period, True
    return scheduled_timestamp, False


def group_notification(existing_notification, user_id, event):
    existing_notification.tours_number += 1
    if not existing_notification.fixed:
        wait_time = predict_wait_time(user_id, event)
        end_period = replace_time(event['date'], config['notifications_time_interval'][1])
        del pending_notification_schedule[(existing_notification.scheduled_timestamp, user_id)]
        scheduled_timestamp, fixed_time = get_scheduled_timestamp(event['date'], end_period, user_id, wait_time)
        existing_notification.scheduled_timestamp = scheduled_timestamp
        existing_notification.fixed = fixed_time
        pending_notification_schedule[(existing_notification.scheduled_timestamp, user_id)] = existing_notification


def handle_single_event(event):
    event_date = event['date']
    user_id = event['user_id']
    notification_start_date = replace_time(event['date'], config['notifications_time_interval'][0])
    notification_end_date = replace_time(event['date'], config['notifications_time_interval'][1])
    if event_date < notification_start_date:
        add_pending_notification(notification_start_date, user_id, event, True)
    elif event_date > notification_end_date:
        next_notification_date = replace_time(event['date'] + datetime.timedelta(days=1),
                                              config['notifications_time_interval'][1])
        add_pending_notification(next_notification_date, user_id, event, True)
    else:
        handle_active_hours_events(event, notification_end_date)


def replace_time(date, time):
    return date.replace(hour=time.hour,
                        minute=time.minute,
                        second=time.second)


def process_event(event):
    user_id = event['user_id']
    if user_id in pending_notification_user:
        group_notification(pending_notification_user[user_id], user_id, event)
    else:
        handle_single_event(event)

    if user_id not in user_daily_events:
        user_daily_events[user_id] = []
    user_daily_events[user_id].append(event['date'])
    if user_id not in user_latest_events:
        user_latest_events[user_id] = []
    user_latest_events[user_id].append(event)


def update_user_model(event):
    user_id = event['user_id']
    timestamp = event['date']
    if user_id in user_latest_events and len(user_latest_events[user_id]) > 0 and notification_handled_stats[
        'days'] > 20:
        hour_events = get_hour_event_frequency(user_latest_events[user_id], timestamp)
        if len(hour_events) > 6:
            prev_hours = frequency_prediction_models[user_id][
                'history'] if user_id in frequency_prediction_models else np.empty((0, 0))
            coef, history = train_model(prev_hours, hour_events)
            frequency_prediction_models[user_id] = {"coef": coef, "history": history}
            user_latest_events[user_id] = []


def get_hour_event_frequency(latest_events, current_timestamp):
    event_frequency = get_hour_range(latest_events[0]['date'], current_timestamp)
    for event in latest_events:
        hour_timestamp = event['date'].replace(minute=0, second=0)
        event_frequency[hour_timestamp] += 1
    return event_frequency


def get_hour_range(latest_event_timestamp, current_timestamp):
    event_frequency = {}
    latest_event_timestamp = latest_event_timestamp.replace(minute=0, second=0)
    last_included_timestamp = current_timestamp.replace(minute=0, second=0) - datetime.timedelta(hours=1)
    for hour in pd.date_range(latest_event_timestamp, last_included_timestamp, freq='H'):
        event_frequency[hour] = 0
    return event_frequency


def change_day(timestamp):
    user_day_notifications.clear()
    user_daily_events.clear()
    return timestamp.date()


def read_events(filename, chunksize):
    job_executor = None
    for data_chunk in pd.read_csv(filename, names=["date", "user_id", "friend_id", "friend_name"], iterator=True,
                                  chunksize=chunksize):
        data_chunk['date'] = data_chunk['date'].apply(pd.to_datetime)
        for index, event in data_chunk.iterrows():
            if job_executor is None:
                job_executor = ScheduledJobsExecutor(event['date'], config['job_timeout'], handle_pending_notifications,
                                                     change_day)
            job_executor.mock_time_passing(event["date"])
            update_user_model(event)
            process_event(event)
    job_executor.job()


if __name__ == "__main__":
    start = time.time()
    read_events(sys.argv[1], 10000)
    end = time.time()
    print("time elapsed is {0}".format(end - start))
